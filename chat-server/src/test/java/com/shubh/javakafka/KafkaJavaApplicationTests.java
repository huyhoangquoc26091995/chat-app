package com.shubh.javakafka;

import com.inviv.chat.ChatApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ChatApplication.class)
class KafkaJavaApplicationTests {

    @Test
    void contextLoads() {
    }

}

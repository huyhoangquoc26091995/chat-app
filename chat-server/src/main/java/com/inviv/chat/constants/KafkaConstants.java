package com.inviv.chat.constants;

public class KafkaConstants {
    public static final String KAFKA_TOPIC = "topic_demo";
    public static final String GROUP_ID = "kafka-sandbox";
    public static final String KAFKA_BROKER = "localhost:9092";
}

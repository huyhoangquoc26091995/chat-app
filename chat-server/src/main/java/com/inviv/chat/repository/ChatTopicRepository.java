package com.inviv.chat.repository;

import com.inviv.chat.entity.ChatTopic;
import com.inviv.chat.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ChatTopicRepository extends JpaRepository<ChatTopic, Long> {

    Optional<ChatTopic> findByTypeAndChatTopicUsers_UserIn(Integer type, List<User> users);

    Optional<ChatTopic> findByIdAndChatTopicUsers_User(Long id, User user);
}

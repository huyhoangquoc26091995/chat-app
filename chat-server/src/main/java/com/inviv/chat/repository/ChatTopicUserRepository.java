package com.inviv.chat.repository;

import com.inviv.chat.entity.ChatTopicUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChatTopicUserRepository extends JpaRepository<ChatTopicUser, Long> {

}

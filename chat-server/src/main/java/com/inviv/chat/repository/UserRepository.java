package com.inviv.chat.repository;

import com.inviv.chat.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String userName);

    Optional<User> findByIdAndActiveStatusAndEnabledTrue(Long id, Integer activeStatus);
}

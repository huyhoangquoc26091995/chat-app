package com.inviv.chat.repository;

import com.inviv.chat.entity.ChatTopicMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ChatTopicMessageRepository extends JpaRepository<ChatTopicMessage, Long> {

    List<ChatTopicMessage> findAllByTopicIdAndCreatedDateGreaterThan(Long topicId, Date createdDate);

    Optional<ChatTopicMessage> findFirstByTopicIdOrderByCreatedDateDesc(Long topicId);

    @Query(nativeQuery = true, value = "SELECT * from chat_topic_message where topic_id = ?1 and created_date > ?2 order by created_date desc limit 1")
    Optional<ChatTopicMessage> findFistByTopicIdAndCreatedDateGreaterThanOrderByCreatedDateDesc(Long topicId, Date createdDate);
}

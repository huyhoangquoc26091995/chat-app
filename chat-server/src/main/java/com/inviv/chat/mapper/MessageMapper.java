package com.inviv.chat.mapper;

import com.inviv.chat.dto.response.MessageResponseDTO;
import com.inviv.chat.model.Message;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MessageMapper {

    MessageResponseDTO toResponseDTO(Message message);

    List<MessageResponseDTO> toResponseDTO(List<Message> message);
}

package com.inviv.chat.mapper;

import com.inviv.chat.dto.UserDTO;
import com.inviv.chat.entity.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User toEntity(UserDTO dto);

    List<User> toEntityList(List<UserDTO> dtoList);

    UserDTO toDTO(User user);

    List<UserDTO> toDTOList(List<User> userList);
}

package com.inviv.chat.dto.response;

import com.inviv.chat.model.File;
import lombok.Data;

import java.util.List;

@Data
public class MessageResponseDTO {
    private String sender;
    private String content;
    private String timestamp;
    private Integer type;
    private List<File> fileUrls;
}

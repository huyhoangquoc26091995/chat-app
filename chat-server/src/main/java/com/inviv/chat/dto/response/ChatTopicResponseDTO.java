package com.inviv.chat.dto.response;

import com.inviv.chat.entity.ChatTopicUser;
import lombok.Data;

import java.util.List;

@Data
public class ChatTopicResponseDTO {

    private String name;

    private Integer type; //1:single 2:group

    private Long adminId; //chủ phòng

    private List<ChatTopicUser> chatTopicUsers;
}

package com.inviv.chat.dto.response;

import lombok.Data;

@Data
public class DirAssignDTO {
    private String fid;
    private String url;
    private String publicUrl;
    private Integer count;
}

package com.inviv.chat.dto;

import lombok.Data;

@Data
public class LoginDTO {

    private String username;

    private String password;

}

package com.inviv.chat.dto;

import lombok.Data;

import java.util.Date;

@Data
public class UserDTO {

    private Long id;

    private Date createdDate;

    private String createdBy;

    private Date modifiedDate;

    private String modifiedBy;

    private String username;

    private String password;

    private String firstName;

    private String lastName;

    private String fullName;

    private String email;

    private String phoneNumber;

    private Date birthday;

    private Long organizationId;

    private Integer gender;

    private Long organizationBranchId;

    private Long employeeId;

    private String employeeCode;

    private String metaData;

    private String uuidLogin;

    private Integer activeStatus;

    private String img;

    private boolean enabled;

}

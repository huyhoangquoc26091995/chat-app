package com.inviv.chat.dto.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@JsonAutoDetect
public class FileResponseDTO {
    private String fid;
    private String name;
    private Integer size;
    private String eTag;
    private String publicUrl;
}

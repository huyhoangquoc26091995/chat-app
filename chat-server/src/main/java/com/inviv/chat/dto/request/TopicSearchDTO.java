package com.inviv.chat.dto.request;

import lombok.Data;

@Data
public class TopicSearchDTO {

    private Long userId;

    private Integer pageSize;

    private Integer pageNumber;

}

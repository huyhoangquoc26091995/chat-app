package com.inviv.chat.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class TopicRequesDTO {

    private Long senderId;

    private Long receiverId;

    private String topicName;

    private Long topicId;

    private List<Long> userIds;
}

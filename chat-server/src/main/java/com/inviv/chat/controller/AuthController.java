package com.inviv.chat.controller;

import com.inviv.chat.dto.LoginDTO;
import com.inviv.chat.entity.User;
import com.inviv.chat.exception.BusinessException;
import com.inviv.chat.security.CustomUserDetails;
import com.inviv.chat.security.Jwt.JwtTokenProvider;
import com.inviv.chat.security.UserDetailServiceImpl;
import com.inviv.chat.security.Jwt.JWTToken;
import com.inviv.chat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private UserService userService;

    @Autowired
    private UserDetailServiceImpl userDetailServiceImpl;

    @PostMapping("/auth/login")
    public ResponseEntity<JWTToken> login(@RequestBody LoginDTO request) {

        User user = userService.login(request.getUsername(), request.getPassword());
        authenticate(request.getUsername(), request.getPassword());
        JWTToken jwtToken = new JWTToken();
        final CustomUserDetails userDetails = userDetailServiceImpl
                .loadUserByUsername(request.getUsername());
        final String token = tokenProvider.generateToken(userDetails);
        jwtToken.setAccessToken(token);
        jwtToken.setUser(user);
        //log.info("login with username: " + request.getUsername());
        return new ResponseEntity<>(jwtToken, HttpStatus.OK);
    }

    private void authenticate(String username, String password) throws DisabledException {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new BusinessException("user disabled");
        } catch (LockedException e) {
            throw new BusinessException("User locked");
        } catch (BadCredentialsException e) {
            throw new BusinessException("Bad Credentials");
        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
    }
}

package com.inviv.chat.controller;

import com.inviv.chat.dto.request.TopicSearchDTO;
import com.inviv.chat.service.chatTopic.ChatTopicService;
import com.inviv.chat.dto.request.TopicRequesDTO;
import com.inviv.chat.entity.ChatTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class TopicController {

    @Autowired
    private ChatTopicService chatTopicService;

    @PostMapping("/chat-topic/get-single-topic")
    public ResponseEntity<ChatTopic> getSingleTopic(@RequestBody TopicRequesDTO request) {

        return ResponseEntity.ok(chatTopicService.getSingleTopic(request));
    }

    @PostMapping("/chat-topic/create-group")
    public ResponseEntity<ChatTopic> createGroup(@RequestBody TopicRequesDTO request) {

        return ResponseEntity.ok(chatTopicService.createGroup(request));
    }

    @PostMapping("/chat-topic/find-by-condition")
    public ResponseEntity<Map<String, Object>> findByCondition(@RequestBody TopicSearchDTO request) {

        return ResponseEntity.ok(chatTopicService.findByCondition(request));
    }
}

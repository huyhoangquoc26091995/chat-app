package com.inviv.chat.controller;

import com.inviv.chat.configuration.WebSocketEventListener;
import com.inviv.chat.dto.response.MessageResponseDTO;
import com.inviv.chat.entity.ChatAttachment;
import com.inviv.chat.enums.MessageTypeEnum;
import com.inviv.chat.mapper.MessageMapper;
import com.inviv.chat.model.File;
import com.inviv.chat.model.Message;
import com.inviv.chat.repository.ChatTopicMessageRepository;
import com.inviv.chat.repository.ChatTopicRepository;
import com.inviv.chat.entity.ChatTopic;
import com.inviv.chat.entity.ChatTopicMessage;
import com.inviv.chat.entity.User;
import com.inviv.chat.exception.BusinessException;
import com.inviv.chat.security.AuthenticationUtils;
import com.inviv.chat.service.storage.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.WebSocketMessage;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@RestController
@Slf4j
public class ChatController {

    @Autowired
    private KafkaTemplate<String, Message> kafkaTemplate;

    @Autowired
    SimpMessagingTemplate template;

    @Autowired
    ChatTopicRepository chatTopicRepository;

    @Autowired
    ChatTopicMessageRepository chatTopicMessageRepository;

    @Autowired
    StorageService storageService;

    @Autowired
    private MessageMapper messageMapper;

    @Autowired
    private WebSocketEventListener webSocketEventListener;

    @PostMapping(value = "/api/send/{topicId}", consumes = "application/json", produces = "application/json")
    public void sendMessage(@RequestBody Message message, @PathVariable("topicId") Long topicId) {

        message.setTimestamp(LocalDateTime.now().toString());

        User sender = AuthenticationUtils.getCurrentUser();
        ChatTopic topic = chatTopicRepository.findByIdAndChatTopicUsers_User(topicId, sender).orElse(null);
        if (topic == null) {
            throw new BusinessException("topic not found");
        }

        ChatTopicMessage chatTopicMessage = new ChatTopicMessage();
        chatTopicMessage.setMessage(message.getContent());
        chatTopicMessage.setSenderId(sender.getId());
        chatTopicMessage.setType(message.getType());
        chatTopicMessage.setTopicId(topic.getId());

        try {
            //Sending the message to kafka topic queue
//            kafkaTemplate.send(KafkaConstants.KAFKA_TOPIC, message).get();
            template.convertAndSend("/topic/" + topicId, message);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        List<ChatAttachment> chatAttachments = new ArrayList<>();
        List<File> fileUrls = new ArrayList<>();
        if (message.getType().equals(MessageTypeEnum.BYTES.getValue()) && message.getFiles().size() > 0) {
            message.getFiles().forEach(file -> {
                ChatAttachment attachment = storageService.uploadFileToSeverFile(file.getData(), file.getFilename());
                chatAttachments.add(attachment);

                File fileUrl = new File();
                fileUrl.setFileUrl(attachment.getUrl());
                fileUrl.setFilename(attachment.getFileName());
                fileUrls.add(fileUrl);
            });
            chatTopicMessage.setFiles(new HashSet<>(chatAttachments));
        }

        chatTopicMessageRepository.save(chatTopicMessage);

        MessageResponseDTO messageResponseDTO = messageMapper.toResponseDTO(message);
        messageResponseDTO.setFileUrls(fileUrls);

    }

    //    -------------- WebSocket API ----------------
    @MessageMapping("/sendMessage/{topic}")
    @SendTo("/topic/{topic}")
    public Message broadcastGroupMessage(@Payload Message message, @DestinationVariable("topic") String topic) {
        //Sending this message to all the subscribers
        String responseMessage = "Received message: " + message.getContent();

        // Gửi tin nhắn trả lời tới chủ đề động được chỉ định từ tham số topic
        return message;
    }

    @MessageMapping("/topic/3")
    public void subscribeTopic(WebSocketMessage message, SimpMessageHeaderAccessor headerAccessor) {
        // Xử lý yêu cầu đăng ký chủ đề từ client
        // Code xử lý yêu cầu đăng ký chủ đề ở đây
        throw new BusinessException("Unauthorized");
    }

    @MessageMapping("/newUser")
    @SendTo("/topic/group")
    public Message addUser(@Payload Message message,
                           SimpMessageHeaderAccessor headerAccessor) {
        // Add user in web socket session
        headerAccessor.getSessionAttributes().put("username", message.getSender());
        return message;
    }

    @SubscribeMapping({"/topic/1", "/topic/2", "/topic/3"})
    public void handleSubscribeMultipleTopics() {
        throw new BusinessException("Error");
    }

    private boolean userHasSubscribePermission(String username, String topic) {

        return false; // Mặc định trả về true cho mục đích minh họa
    }

}

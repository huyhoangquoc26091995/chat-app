package com.inviv.chat.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = -609099771903107222L;

    private List<Object> lstParam;
    private Integer status;
    private Map<String, String> errors;

    public BusinessException(String message, Integer status) {
        super(message);
        this.status = status;
    }

    public BusinessException(String message, Integer status, Map<String, String> errors) {
        super(message);
        this.status = status;
        this.errors = errors;
    }

    public BusinessException(String arg0) {
        super(arg0);
    }

    public BusinessException(String arg0, Object... params) {
        super(arg0);
        lstParam = new ArrayList<>();
        Collections.addAll(lstParam, params);
    }
}

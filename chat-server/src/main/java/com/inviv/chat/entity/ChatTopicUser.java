package com.inviv.chat.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "chat_topic_user")
public class ChatTopicUser extends BaseEntity{

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "chat_topic_id", nullable = false)
    private ChatTopic chatTopic;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "deleted_at")
    private Date deletedAt;
}

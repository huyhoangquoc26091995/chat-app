package com.inviv.chat.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Data
@Entity
@Table(name = "chat_topic_message")
public class ChatTopicMessage extends BaseEntity{

    @Column(name = "topic_id")
    private Long topicId;

    @Column(name = "message")
    private String message;

    @Column(name = "type")
    private Integer type; //1:text 2:files

    @Column(name = "sender_id")
    private Long senderId;

    @Column(name = "send_at")
    private Date sendAt;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "message_attachment",
            joinColumns =
            @JoinColumn(name = "message_id", referencedColumnName = "id"),
            inverseJoinColumns =
            @JoinColumn(name = "attachment_id", referencedColumnName = "id"))
    private Set<ChatAttachment> files;
}

package com.inviv.chat.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "chat_attachment")
public class ChatAttachment extends BaseEntity{

    @Column(name = "message_id")
    private String messageId;

    @Column(name = "file_id", nullable = false)
    private String fileId;

    @Column(name = "file_name", nullable = false)
    private String fileName;

    @Column(name = "url")
    private String url;

    @Column(name = "size")
    private Integer size;

}

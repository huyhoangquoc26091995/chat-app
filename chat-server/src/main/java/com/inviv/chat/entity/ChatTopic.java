package com.inviv.chat.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "chat_topic")
public class ChatTopic extends BaseEntity{

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private Integer type; //1:single 2:group

    @Column(name = "admin_id")
    private Long adminId; //chủ phòng

    @OneToMany(mappedBy = "chatTopic", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ChatTopicUser> chatTopicUsers;

}

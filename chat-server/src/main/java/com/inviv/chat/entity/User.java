package com.inviv.chat.entity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

/**
 * Author: truonglx.93
 * <p>
 * Date: 2019-12-02
 */
@Getter
@Setter
@Entity
@Table(name = "emb_user", uniqueConstraints = {@UniqueConstraint(columnNames = {"username"})})
public class User extends BaseEntity {

    @Column(name = "username")
    @NotNull
    @NotEmpty
    private String username;

    @Column(name = "password")
    @NotNull
    @NotEmpty
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Column(name = "email", nullable = true)
    @Email(message = "{ValidEmail.user.email}")
    private String email;

    @Column(name = "phone_number", length = 20)
    private String phoneNumber;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "organization_id")
    private Long organizationId;

    @Column(name = "gender")
    private Integer gender;

    @Column(name = "organization_branch_id")
    private Long organizationBranchId;

    @Column(name = "employee_id")
    private Long employeeId;

    @Column(name = "employee_code")
    private String employeeCode;

    @Column(name = "metadata", columnDefinition = "TEXT", length = Integer.MAX_VALUE)
    private String metaData;

    @Column(name = "uuid_login")
    private String uuidLogin;

    @Column(name = "active_status")
    private Integer activeStatus;

    @Column(name = "img")
    private String img;

    private boolean enabled;

    //Set all account non expired
    private final boolean accountNonExpired = true;

    //Set all account no locked
    private final boolean accountNonLocked = true;

    //Set all account credentials  non expired
    private final boolean credentialsNonExpired = true;

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return username.equals(user.username);
    }

    @Override
    public int hashCode() {

        return Objects.hash(username);
    }
}

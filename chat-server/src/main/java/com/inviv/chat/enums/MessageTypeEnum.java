package com.inviv.chat.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MessageTypeEnum {
    TEXT(1, "Text"),
    BYTES(2, "Bytes"),
    ;

    private Integer value;
    private String sValue;
}

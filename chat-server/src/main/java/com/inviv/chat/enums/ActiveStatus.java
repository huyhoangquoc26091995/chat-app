package com.inviv.chat.enums;

/**
 * @author : truonglx
 * @version : 05/01/2019
 */

public enum ActiveStatus {
    INACTIVE(0, "InActive"),
    ACTIVE(1, "Active"),
    LOCKED(2, "Locked");

    private int value;

    private String sValue;

    ActiveStatus(int value, String sValue) {

        this.value = value;
        this.sValue = sValue;
    }

    public static ActiveStatus parse(int value) {

        for (ActiveStatus gender : ActiveStatus.values()) {
            if (gender.value == value) {
                return gender;
            }
        }
        return INACTIVE;
    }

    public static ActiveStatus parse(String value) {

        for (ActiveStatus gender : ActiveStatus.values()) {
            if (gender.sValue.equalsIgnoreCase(value)) {
                return gender;
            }
        }
        return INACTIVE;
    }

    public int getValue() {

        return value;
    }

    public String getsValue() {

        return sValue;
    }
}

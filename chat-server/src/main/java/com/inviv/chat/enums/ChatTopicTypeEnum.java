package com.inviv.chat.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ChatTopicTypeEnum {
    SINGLE(1, "Đơn"),
    GROUP(2, "Nhóm"),
    ;

    private Integer value;
    private String sValue;
}

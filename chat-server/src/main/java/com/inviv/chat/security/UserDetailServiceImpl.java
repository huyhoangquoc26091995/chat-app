package com.inviv.chat.security;

import com.inviv.chat.entity.User;
import com.inviv.chat.exception.BusinessException;
import com.inviv.chat.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserRepository userRepo;

    private final HttpSession session;

    private final MessageSource messageSource;


    @Override
    public CustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username).orElse(null);
        if (user == null) {
            //log.error("User not found! " + username);
            throw new BusinessException("User not found");
        }
        // Add info logged user to Session
        session.setAttribute("user", user);
        List<GrantedAuthority> grantList = new ArrayList<>();
//        List<RoleDTO> roles = roleRepoCustom.getRoles(userMapper.toDTO(user));
//        if(!roles.isEmpty()){
//            List<String> roleNames = roles.stream().map(RoleDTO::getCode).collect(Collectors.toList());
//            for (String role : roleNames) {
//                GrantedAuthority authority = new SimpleGrantedAuthority(role);
//                grantList.add(authority);
//            }
//        }
        return new CustomUserDetails(user, grantList);
    }
}

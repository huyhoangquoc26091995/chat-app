package com.inviv.chat.security;

import com.inviv.chat.entity.User;
import com.inviv.chat.mapper.UserMapper;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@AllArgsConstructor
public class AuthenticationUtils {

    @Autowired
    UserMapper userMapper;

    public static User getCurrentUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication.isAuthenticated()) {
            // Lấy thông tin người dùng hiện tại
            CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
            return userDetails.getUser();

        } else {
            return null;
        }
    }
}

package com.inviv.chat.model;

import lombok.Data;

import java.util.List;

@Data
public class Message {
    private String sender;
    private String content;
    private String timestamp;
    private List<File> files;
    private Integer type;

}

package com.inviv.chat.model;

import lombok.Data;

@Data
public class WebsocketSessionAttribute {
    private String sessionId;
    private String username;
}

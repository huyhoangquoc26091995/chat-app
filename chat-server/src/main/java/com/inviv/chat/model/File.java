package com.inviv.chat.model;

import lombok.Data;

@Data
public class File {
    private byte[] data;
    private String filename;
    private String fileUrl;
}

package com.inviv.chat.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inviv.chat.entity.ChatTopic;
import com.inviv.chat.entity.User;
import com.inviv.chat.exception.BusinessException;
import com.inviv.chat.model.WebsocketSessionAttribute;
import com.inviv.chat.repository.ChatTopicRepository;
import com.inviv.chat.repository.UserRepository;
import com.inviv.chat.security.AuthenticationUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class WebSocketEventListener {

    private final Map<String, String> connectedClients = new ConcurrentHashMap<>();

    private final ObjectMapper objectMapper;

    private final ChatTopicRepository chatTopicRepository;

    private final UserRepository userRepository;

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
        String sessionId = headerAccessor.getSessionId();
        WebsocketSessionAttribute attributes = objectMapper.convertValue(headerAccessor.getHeader("simpSessionAttributes"), WebsocketSessionAttribute.class);

        // Add the connected client to the map
        assert attributes != null;
        connectedClients.put(sessionId, attributes.getUsername());

        // You can use the connectedClients map for any further processing or broadcasting
    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
        String sessionId = headerAccessor.getSessionId();
        String username = connectedClients.get(sessionId);

        // Remove the disconnected client from the map
        connectedClients.remove(sessionId);

        // Handle the disconnect event

        // You can use the connectedClients map for any further processing or broadcasting
    }

    public List<String> getConnectedUser() {
        return connectedClients.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
    }

//    @EventListener
//    public void handleWebSocketSubscribeListener(SessionSubscribeEvent event) {
//
//        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
//        String topicName = extractTopicName(Objects.requireNonNull(headerAccessor.getDestination()));
//        WebsocketSessionAttribute attributes = objectMapper.convertValue(headerAccessor.getHeader("simpSessionAttributes"), WebsocketSessionAttribute.class);
//
//        User user = userRepository.findByUsername(attributes.getUsername()).orElse(null);
//        if(user == null){
//            throw new BusinessException("User not found");
//        }
//
//        ChatTopic chatTopic = chatTopicRepository.findByIdAndChatTopicUsers_User(Long.valueOf(topicName), user).orElse(null);
//        if(chatTopic == null){
//            throw new BusinessException("No permission to subscribe topic");
//        }
//
//    }
//
//    @EventListener
//    public void handleWebSocketUnsubscribe(SessionUnsubscribeEvent event) {
//
//        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
//        String topicName = extractTopicName(headerAccessor.getHeader("simpSubscriptionId").toString());
//        WebsocketSessionAttribute attributes = objectMapper.convertValue(headerAccessor.getHeader("simpSessionAttributes"), WebsocketSessionAttribute.class);
//
//        User user = userRepository.findByUsername(attributes.getUsername()).orElse(null);
//        if(user == null){
//            throw new BusinessException("User not found");
//        }
//
//        ChatTopic chatTopic = chatTopicRepository.findByIdAndChatTopicUsers_User(Long.valueOf(topicName), user).orElse(null);
//        if(chatTopic == null){
//            throw new BusinessException("No permission to unsubscribe topic");
//        }
//
//    }

    private boolean hasSubscribeTopicAuthority(Authentication auth) {
        // Kiểm tra xem người dùng có quyền SUBSCRIBE_TOPIC hay không
        return auth.getAuthorities().stream()
                .anyMatch(authority -> authority.getAuthority().equals("SUBSCRIBE_TOPIC"));
    }

    private String extractTopicName(String destination) {
        // Chuyển đổi đường dẫn chủ đề thành tên chủ đề
        // Ví dụ: Nếu destination là "/topic/chat", thì trả về "chat"
        if (destination.startsWith("/topic/")) {
            return destination.substring("/topic/".length());
        }
        return destination;
    }
}

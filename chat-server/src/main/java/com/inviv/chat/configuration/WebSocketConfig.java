package com.inviv.chat.configuration;

import com.inviv.chat.security.CustomUserDetails;
import com.inviv.chat.security.Jwt.JwtTokenProvider;
import com.inviv.chat.security.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;
import org.springframework.web.socket.server.HandshakeInterceptor;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import java.util.Map;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private UserDetailServiceImpl userDetailServiceImpl;

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        // chat client will use this to connect to the server
        registry.addEndpoint("/ws-chat")
                .setAllowedOrigins("*")
                .addInterceptors(httpSessionHandshakeInterceptor())
                .addInterceptors(webSocketHandshakeInterceptor())
                .withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {

        //cho phép client gửi tin nhắn đến server với tiền tố "/app".
        registry.setApplicationDestinationPrefixes("/app");

        //cho phép client đăng ký các chủ đề (topics) với tiền tố "/topic"
        registry.enableSimpleBroker("/topic");
    }


    private HandshakeInterceptor httpSessionHandshakeInterceptor() {
        return new HttpSessionHandshakeInterceptor();
    }

    private HandshakeInterceptor webSocketHandshakeInterceptor() {
        return new HandshakeInterceptor() {
            @Override
            public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {


                // Kiểm tra token được gửi từ client
                String token = request.getURI().getQuery();
                if (isValidToken(token)) {
                    String username = extractUsernameFromToken(token);
                    CustomUserDetails userDetails = userDetailServiceImpl.loadUserByUsername(username);
                    jwtTokenProvider.validateToken(token, userDetails);
                    // Token hợp lệ, lưu trữ thông tin xác thực trong session
                    String sessionId = request.getHeaders().getFirst("sec-websocket-key");
                    attributes.put("sessionId", sessionId);
                    attributes.put("username", username);
                    return true;
                } else {
                    // Token không hợp lệ, từ chối kết nối WebSocket
                    response.setStatusCode(HttpStatus.FORBIDDEN);
                    return false;
                }
            }

            @Override
            public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception ex) {
                // Nothing to do after handshake
            }
        };
    }

    @Override
    public void configureWebSocketTransport(WebSocketTransportRegistration registration) {
        registration.setMessageSizeLimit(2 * 1024 * 1024); //2MB
        registration.setSendBufferSizeLimit(100 * 1024 * 1024); //100MB
    }

    private boolean isValidToken(String token) {
        // Kiểm tra tính hợp lệ của token, có thể kiểm tra đối với một cơ sở dữ liệu hoặc hệ thống xác thực nào đó
        // Trong ví dụ này, giả sử token hợp lệ nếu tồn tại và không rỗng
        return token != null && !token.isEmpty();
    }

    private String extractUsernameFromToken(String token) {
        return jwtTokenProvider.getUsernameFromToken(token);
    }
}

package com.inviv.chat.service;

import com.inviv.chat.entity.User;

public interface UserService {
    User login(String username, String password);
}

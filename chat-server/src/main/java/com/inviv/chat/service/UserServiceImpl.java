package com.inviv.chat.service;

import com.inviv.chat.entity.User;
import com.inviv.chat.exception.BusinessException;
import com.inviv.chat.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService{

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;


    @Override
    public User login(String username, String password) {
        Optional<User> optional = userRepository.findByUsername(username);

        if (!optional.isPresent()) {
            throw new BusinessException("User name invalid");
        }
        User user = optional.get();
        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new BusinessException("Password invalid");
        }

        return user;
    }

}

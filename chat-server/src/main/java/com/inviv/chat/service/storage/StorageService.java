package com.inviv.chat.service.storage;

import com.google.api.gax.rpc.NotFoundException;
import com.inviv.chat.dto.response.FileResponseDTO;
import com.inviv.chat.entity.ChatAttachment;

import java.io.IOException;

public interface StorageService {

    FileResponseDTO write(byte[] data, String fileName) throws IOException, NotFoundException;

    ChatAttachment uploadFileToSeverFile(byte[] file, String fileName);
}

package com.inviv.chat.service.chatTopic;

import com.inviv.chat.dto.request.TopicSearchDTO;
import com.inviv.chat.entity.ChatTopicMessage;
import com.inviv.chat.repository.ChatTopicMessageRepository;
import com.inviv.chat.repository.ChatTopicRepository;
import com.inviv.chat.dto.request.TopicRequesDTO;
import com.inviv.chat.entity.ChatTopic;
import com.inviv.chat.entity.ChatTopicUser;
import com.inviv.chat.entity.User;
import com.inviv.chat.enums.ActiveStatus;
import com.inviv.chat.enums.ChatTopicTypeEnum;
import com.inviv.chat.exception.BusinessException;
import com.inviv.chat.repository.ChatTopicUserRepository;
import com.inviv.chat.repository.UserRepository;
import com.inviv.chat.security.AuthenticationUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class ChatTopicServiceImpl implements ChatTopicService {

    private final ChatTopicRepository chatTopicRepository;

    private final UserRepository userRepository;

    private final ChatTopicUserRepository chatTopicUserRepository;

    private final EntityManager em;

    private final ChatTopicMessageRepository chatTopicMessageRepository;

    @Override
    public Map<String, Object> findByCondition(TopicSearchDTO dto) {
        List<ChatTopic> chatTopics;
        CriteriaBuilder cb = em.getCriteriaBuilder();

        User user = AuthenticationUtils.getCurrentUser();

        CriteriaQuery<ChatTopic> cq = cb.createQuery(ChatTopic.class);
        Root<ChatTopic> root = cq.from(ChatTopic.class);
        Join<ChatTopic, ChatTopicUser> joinTopicUser = root.join("chatTopicUsers", JoinType.INNER);

        List<Predicate> predicates = new ArrayList<>();

        predicates.add(cb.equal(joinTopicUser.get("user"), user));
        cq.orderBy(cb.desc(root.get("modifiedDate")));

        int total = em.createQuery(cq.select(root)
                .where(cb.and(predicates.toArray(new Predicate[0])))).getResultList().size();

        if (dto.getPageNumber() != null && dto.getPageSize() != null) {
            CriteriaQuery<ChatTopic> select = cq.select(root);
            TypedQuery<ChatTopic> tq = em.createQuery(select);
            tq.setFirstResult((dto.getPageNumber() - 1) * dto.getPageSize());
            tq.setMaxResults(dto.getPageSize());
            chatTopics = tq.getResultList();
        } else {
            chatTopics = em.createQuery(cq.select(root)
                    .where(cb.and(predicates.toArray(new Predicate[0])))).getResultList();
        }

        chatTopics.forEach(chatTopic -> {
            ChatTopicUser chatTopicUser = chatTopic.getChatTopicUsers().stream().filter(x -> x.getUser().getId().equals(user.getId())).findFirst().orElse(null);
            if (chatTopicUser != null && chatTopicUser.getDeletedAt() != null) {
                ChatTopicMessage chatTopicMessage = chatTopicMessageRepository.findFistByTopicIdAndCreatedDateGreaterThanOrderByCreatedDateDesc(chatTopic.getId(), chatTopicUser.getDeletedAt()).orElse(null);

            } else {
                ChatTopicMessage chatTopicMessage = chatTopicMessageRepository.findFirstByTopicIdOrderByCreatedDateDesc(chatTopic.getId()).orElse(null);
            }
        });
        Map<String, Object> map = new HashMap<>();
        map.put("data", chatTopics);
        map.put("total", total);

        return map;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ChatTopic getSingleTopic(TopicRequesDTO dto) {
        List<User> topicUsers = new ArrayList<>();
        dto.getUserIds().forEach(id -> {
            User user = userRepository.findByIdAndActiveStatusAndEnabledTrue(id, ActiveStatus.ACTIVE.getValue()).orElse(null);
            if (user == null) {
                throw new BusinessException("user not found");
            }
            topicUsers.add(user);
        });

        ChatTopic chatTopic = chatTopicRepository.findByTypeAndChatTopicUsers_UserIn(ChatTopicTypeEnum.SINGLE.getValue(), topicUsers).orElse(null);

        if (chatTopic == null) {
            chatTopic = new ChatTopic();
            chatTopic.setType(ChatTopicTypeEnum.SINGLE.getValue());
            chatTopic = chatTopicRepository.save(chatTopic);

            List<ChatTopicUser> chatTopicUsers = new ArrayList<>();
            ChatTopic finalChatTopic = chatTopic;

            topicUsers.forEach(user -> {
                ChatTopicUser chatTopicUser = new ChatTopicUser();
                chatTopicUser.setDeleted(false);
                chatTopicUser.setUser(user);
                chatTopicUser.setChatTopic(finalChatTopic);

                chatTopicUsers.add(chatTopicUser);
            });
            finalChatTopic.setChatTopicUsers(chatTopicUserRepository.saveAll(chatTopicUsers));
        }

        return chatTopic;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ChatTopic createGroup(TopicRequesDTO dto) {
        List<User> topicUsers = new ArrayList<>();
        dto.getUserIds().forEach(id -> {
            User user = userRepository.findByIdAndActiveStatusAndEnabledTrue(id, ActiveStatus.ACTIVE.getValue()).orElse(null);
            if (user == null) {
                throw new BusinessException("user not found");
            }
            topicUsers.add(user);
        });

        ChatTopic chatTopic = new ChatTopic();
        chatTopic.setName(dto.getTopicName());
        chatTopic.setType(ChatTopicTypeEnum.GROUP.getValue());
        chatTopic.setAdminId(AuthenticationUtils.getCurrentUser().getId());

        ChatTopic finalChatTopic = chatTopicRepository.save(chatTopic);

        List<ChatTopicUser> chatTopicUsers = new ArrayList<>();
        topicUsers.forEach(user -> {
            ChatTopicUser chatTopicUser = new ChatTopicUser();
            chatTopicUser.setDeleted(false);
            chatTopicUser.setUser(user);
            chatTopicUser.setChatTopic(finalChatTopic);

            chatTopicUsers.add(chatTopicUser);
        });
        finalChatTopic.setChatTopicUsers(chatTopicUserRepository.saveAll(chatTopicUsers));

        return finalChatTopic;
    }
}

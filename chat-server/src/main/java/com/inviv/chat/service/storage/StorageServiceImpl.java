package com.inviv.chat.service.storage;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.gax.rpc.NotFoundException;
import com.inviv.chat.dto.response.DirAssignDTO;
import com.inviv.chat.dto.response.FileResponseDTO;
import com.inviv.chat.entity.ChatAttachment;
import com.inviv.chat.exception.BusinessException;
import com.inviv.chat.repository.ChatAttachmentRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Service
public class StorageServiceImpl implements StorageService {

    @Value("${host.file-public-url}")
    private String publicURl;

    @Value("${host.file-default}")
    private String serverFileDefault;

    @Value("${host.file-action}")
    private String serverFileAction;

    private final ChatAttachmentRepository chatAttachmentRepository;

    public StorageServiceImpl(ChatAttachmentRepository chatAttachmentRepository) {
        this.chatAttachmentRepository = chatAttachmentRepository;
    }

    @Override
    public FileResponseDTO write(byte[] data, String fileName) throws IOException, NotFoundException {
        RestTemplate restTemplate = new RestTemplate();
        String urlDirAssign
                = serverFileDefault + "/dir/assign";
        HttpEntity<DirAssignDTO> request = new HttpEntity<>(new DirAssignDTO());
        ResponseEntity<DirAssignDTO> response = restTemplate
                .exchange(urlDirAssign, HttpMethod.POST, request, DirAssignDTO.class);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new BusinessException("Upload file error.");
        }
        DirAssignDTO dirAssignDTOResponse = response.getBody();

        String urlSendFile = serverFileAction + "/" + dirAssignDTOResponse.getFid();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body
                = new LinkedMultiValueMap<>();
        body.add("file", new ByteArrayResource(data));

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, headers);
        RestTemplate restTemplateSendFile = new RestTemplate();
        ResponseEntity<String> responseSendFile = restTemplateSendFile
                .postForEntity(urlSendFile, requestEntity, String.class);
        if (responseSendFile.getStatusCode() != HttpStatus.CREATED) {
            throw new BusinessException("Upload file error.");
        }

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(responseSendFile.getBody());

        FileResponseDTO writeFileResponseDTO = new FileResponseDTO();
        writeFileResponseDTO.setFid(dirAssignDTOResponse.getFid());
        writeFileResponseDTO.setName(fileName);
        writeFileResponseDTO.setSize(Integer.parseInt(root.path("size").toString()));
        writeFileResponseDTO.setETag(root.path("eTag") + "");
        writeFileResponseDTO.setPublicUrl(publicURl);
        return writeFileResponseDTO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ChatAttachment uploadFileToSeverFile(byte[] file, String fileName) {

        ChatAttachment chatAttachment;
        try {
            FileResponseDTO fileResponseDTO = this.write(file, fileName);

            chatAttachment = new ChatAttachment();
            chatAttachment.setFileId(fileResponseDTO.getFid());
            chatAttachment.setFileName(fileResponseDTO.getName());
            chatAttachment.setSize(fileResponseDTO.getSize());
            chatAttachment.setUrl(fileResponseDTO.getPublicUrl() + fileResponseDTO.getFid());

            chatAttachment = chatAttachmentRepository.save(chatAttachment);

        } catch (IOException e) {
            throw new BusinessException("Upload file error");
        }

        return chatAttachment;
    }
}

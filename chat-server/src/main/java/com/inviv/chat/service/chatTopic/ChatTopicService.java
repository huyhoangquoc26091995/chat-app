package com.inviv.chat.service.chatTopic;

import com.inviv.chat.dto.request.TopicRequesDTO;
import com.inviv.chat.dto.request.TopicSearchDTO;
import com.inviv.chat.entity.ChatTopic;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

public interface ChatTopicService {
    Map<String, Object> findByCondition(TopicSearchDTO dto);

    ChatTopic getSingleTopic(TopicRequesDTO dto);

    ChatTopic createGroup(TopicRequesDTO dto);
}

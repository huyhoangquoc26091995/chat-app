//package com.inviv.chat.consumer;
//
//import com.inviv.chat.model.Message;
//import com.inviv.chat.constants.KafkaConstants;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.messaging.simp.SimpMessagingTemplate;
//import org.springframework.stereotype.Component;
//
//@Component
//public class MessageListener {
//    @Autowired
//    SimpMessagingTemplate template;
//
//    @KafkaListener(
//            topics = KafkaConstants.KAFKA_TOPIC,
//            groupId = KafkaConstants.GROUP_ID
//    )
//    public void listen(Message message) {
//        System.out.println("sending via kafka listener..");
//        template.convertAndSend("/topic/group", message);
//    }
//}

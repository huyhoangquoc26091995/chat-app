import Axios from "axios";

const api = Axios.create({
    baseURL: '/api/',
    headers: {
        Authorization: 'Bearer ' + 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIwOTIyMjIyMjIyQGdtYWlsLmNvbSIsImV4cCI6MTY5MDA5MTA4NywiaWF0IjoxNjkwMDQ3ODg3fQ.IMeiCEkrUT1HsWKzSj71Kt8mV7I73JVKTdoAqVzKLcqjHRE6RSzZ5_d_inp0gxgJVPX9Tglj2jW9umzS6uThrA'
    }
});

const chatAPI = {
    getMessages: (groupId) => {
        console.log('Calling get messages from API');
        return api.get(`messages/${groupId}`);
    },

    sendMessage: (username, text) => {
        let msg = {
            sender: username,
            content: text
        }
        return api.post(`send`, msg);
    }
}


export default chatAPI;

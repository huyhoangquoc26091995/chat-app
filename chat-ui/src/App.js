import React, {useEffect, useState} from 'react';
import './App.css';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import Axios from "axios";

const token = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIwOTIyMjIyMjIyQGdtYWlsLmNvbSIsImV4cCI6MTY5MDIyMDU2MSwiaWF0IjoxNjkwMTc3MzYxfQ.t-aesx8oCNOjAUkzj3ep6Stsn9eK9EpAe7Z9Z-0NQ_mierYFZdf_uXoZDb5rRqXDST3ZfKnasxPBVNdb3KAfeg';
const SOCKET_URL = 'http://localhost:8080/ws-chat?' + token;
const headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + token
};

const App = () => {
    // const [messages, setMessages] = useState([])
    // const [user, setUser] = useState(null)
    //
    //
    // let onConnected = () => {
    //   console.log("Connected!!")
    // }
    //
    // let onMessageReceived = (msg) => {
    //   console.log('New Message Received!!', msg);
    //   setMessages(messages.concat(msg));
    // }
    //
    // let onSendMessage = (msgText) => {
    //   chatAPI.sendMessage(user.username, msgText).then(res => {
    //     console.log('Sent', res);
    //   }).catch(err => {
    //     console.log('Error Occured while sending message to api');
    //   })
    // }
    //
    // let handleLoginSubmit = (username) => {
    //   console.log(username, " Logged in..");
    //
    //   setUser({
    //     username: username,
    //     color: randomColor()
    //   })
    //
    // }
    //
    // let header = {
    //   Authorization: 'Bearer ' + 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIwOTIyMjIyMjIyQGdtYWlsLmNvbSIsImV4cCI6MTY5MDA5MTA4NywiaWF0IjoxNjkwMDQ3ODg3fQ.IMeiCEkrUT1HsWKzSj71Kt8mV7I73JVKTdoAqVzKLcqjHRE6RSzZ5_d_inp0gxgJVPX9Tglj2jW9umzS6uThrA'
    // }
    //
    // return (
    //   <div className="App">
    //     {!!user ?
    //       (
    //         <>
    //           <SockJsClient
    //             url={SOCKET_URL}
    //             topics={['/topic/' + {user}]}
    //             onConnect={onConnected}
    //             onDisconnect={console.log("Disconnected!")}
    //             onMessage={msg => onMessageReceived(msg)}
    //             debug={false}
    //           />
    //           <Messages
    //             messages={messages}
    //             currentUser={user}
    //           />
    //           <Input onSendMessage={onSendMessage} />
    //         </>
    //       ) :
    //       <LoginForm onSubmit={handleLoginSubmit} />
    //     }
    //   </div>
    // )

    const api = Axios.create({
        headers: {
            Authorization: 'Bearer ' + token
        }
    });

    const [message, setMessage] = useState('');
    const [receivedMessage, setReceivedMessage] = useState('');
    const [dynamicTopic, setDynamicTopic] = useState('');
    const [stompClient, setStompClient] = useState('');
    useEffect(() => {
        // Kết nối đến WebSocket server

        const socket = new SockJS('http://localhost:8080/ws-chat?' + token);
        const stompClient = Stomp.over(socket);
        // Khi kết nối thành công, đăng ký để nhận tin nhắn từ chủ đề động
        stompClient.connect({}, () => {
            // Định nghĩa chủ đề động tùy ý (ví dụ: "user/{userId}")
            const dynamicTopic = '';
            stompClient.subscribe(`/topic/3`, (response) => {
                // Xử lý thông điệp từ server
                console.log("suscibe topic 3" + response.body);
                const message = JSON.parse(response.body);
                setReceivedMessage(message.content != null && message.content !== '' ? message.content : message.fileUrls[0].fileUrl);
                setFileName(message.fileUrls != null && message.fileUrls.length > 0 ? message.fileUrls[0].filename : "");
            }, headers);

            // stompClient.unsubscribe(`/topic/3`, (response) => {
            //     // Xử lý thông điệp từ server
            //     console.log("suscibe topic 3" + response.body);
            //     const message = JSON.parse(response.body);
            //     setReceivedMessage(message.content != null && message.content !== '' ? message.content : message.fileUrls[0].fileUrl);
            //     setFileName(message.fileUrls != null && message.fileUrls.length > 0 ? message.fileUrls[0].filename : "");
            // }, headers);
        });

        //Hủy kết nối WebSocket khi component unmount
        return () => {
            stompClient.disconnect();
        };

    }, [dynamicTopic]);

    const handleSendMessage = () => {

        // Gửi thông điệp đến server

        const socket = new SockJS('http://localhost:8080/ws-chat?' + token);
        const stompClient = Stomp.over(socket);

        let messageType = 1;
        stompClient.connect({}, () => {
            let files = [];
            if (fileBytes) {
                messageType = 2;
                // Convert the Uint8Array to a regular array to send it as JSON
                const fileBytesArray = Array.from(fileBytes.byte);

                // Create the payload to be sent to the server
                files = [{
                    data: fileBytesArray,
                    filename: fileBytes.name
                }];
            }
            const messageObject = {
                content: message,
                files: files,
                type: messageType
            };

            api.post('http://localhost:8080/api/send/' + dynamicTopic, messageObject).then(r => {
                console.log(r);
            })
            //stompClient.send('/app/sendMessage', {}, JSON.stringify(messageObject));
        });
    };

    const [fileBytes, setFileBytes] = useState(null);

    const [fileName, setFileName] = useState(null);

    const handleFileInputChange = (event) => {
        const file = event.target.files[0];

        // Create a FileReader
        const reader = new FileReader();

        // When the FileReader has finished reading the file
        reader.onloadend = () => {
            // Get the result as a Uint8Array
            const fileDataArray = new Uint8Array(reader.result);

            // Set the file bytes to the state
            setFileBytes({byte: fileDataArray, name: file.name});
        };

        // Read the file as a Blob
        reader.readAsArrayBuffer(file);
    };

    return (
        <div>
            <input
                type="text"
                placeholder="Dynamic Topic (e.g., user/{userId})"
                value={dynamicTopic}
                onChange={(e) => setDynamicTopic(e.target.value)}
            />
            <input
                type="text"
                placeholder="Message"
                value={message}
                onChange={(e) => setMessage(e.target.value)}
            />

            <input type="file" onChange={handleFileInputChange}/>

            <button onClick={handleSendMessage}>Send</button>
            <div>Received Message: {receivedMessage}</div>
            <div>FileName: {fileName}</div>
        </div>
    );
}

export default App;
